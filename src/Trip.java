package src;

import java.util.ArrayList;
import java.util.List;

public class Trip implements Comparable {

    private final String TripType;
    private double TripValue;
    private List<Vertex> path = new ArrayList<>();

    public Trip(String tripType) {
        TripType = tripType;
    }

    public void setTripValue(double tripValue) {
        TripValue = tripValue;
    }

    public void setPath(List<Vertex> path) {
        this.path = path;
    }

    @Override
    public String toString() {
        String tripString = switch (TripType) {
            case "car" -> "Trip was " + TripValue + " km long";
            case "flight" -> "Trip cost was €" + TripValue;
            case "train" -> "Trip took " + TripValue + " minutes";
            default -> "";
        };

        return "Trip " +
                "Type of trip :" + TripType + "" + tripString;

    }
}
