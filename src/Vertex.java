package src;

import java.util.ArrayList;
import java.util.List;

public class Vertex {

    private List<Step> steps;
    private Vertex previousVertex;
    private double minDistance = 0.0;

    public Vertex() {
        this.steps = new ArrayList<>();
    }

    public void setStep(Step step) {
        this.steps.add(step);
    }

    public List<Step> getSteps() {
        return steps;
    }

    public Vertex getPreviousVertex() {
        return previousVertex;
    }

    public void setPreviousVertex(Vertex previousVertex) {
        this.previousVertex = previousVertex;
    }

    public double getMinDistance() {
        return minDistance;
    }

    public void setMinDistance(double minDistance) {
        this.minDistance = minDistance;
    }
}
