package src;

public class Flight extends Step {

    private double price;

    public Flight(Vertex source, Vertex destination, int weight, double price) {
        super(source, destination, weight);
        this.price = price;
    }
}
