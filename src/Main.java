package src;

public class Main {

    public static void main(String[] args) {

        Dijkstra dk = new Dijkstra();

        // Car
        Vertex v1 = new Vertex();
        Vertex v2 = new Vertex();
        Vertex v3 = new Vertex();
        Vertex v4 = new Vertex();
        Vertex v5 = new Vertex();

        v1.setStep(new Step(v1, v2, 3));
        v1.setStep(new Step(v1, v3, 5));
        v2.setStep(new Step(v2, v1, 3));
        v2.setStep(new Step(v2, v5, 2));
        v3.setStep(new Step(v3, v1, 5));
        v3.setStep(new Step(v3, v4, 2));
        v4.setStep(new Step(v4, v3, 2));
        v4.setStep(new Step(v4, v5, 2));
        v5.setStep(new Step(v5, v4, 2));
        v5.setStep(new Step(v5, v2, 2));

        Trip CarTrip = new Trip("car");
        CarTrip.setTripValue(dk.getPathDis(v5));
        CarTrip.setPath(dk.getPath(v3));
        System.out.println(CarTrip);

        // Flight
        Vertex f1 = new Vertex();
        Vertex f2 = new Vertex();
        Vertex f3 = new Vertex();
        Vertex f4 = new Vertex();
        Vertex f5 = new Vertex();

        f1.setStep(new Step(f1, f2, 300));
        f1.setStep(new Step(f1, f4, 520));
        f2.setStep(new Step(f2, f3, 340));
        f2.setStep(new Step(f2, f5, 220));
        f3.setStep(new Step(f3, f4, 510));
        f3.setStep(new Step(f3, f4, 227));
        f4.setStep(new Step(f4, f3, 224));
        f4.setStep(new Step(f4, f2, 453));
        f5.setStep(new Step(f5, f1, 275));
        f5.setStep(new Step(f5, f2, 387));

        Trip flightTrip = new Trip("flight");
        flightTrip.setTripValue(dk.getPathDis(v5));
        flightTrip.setPath(dk.getPath(v3));
        System.out.println(flightTrip);

        // Train
        Vertex r1 = new Vertex();
        Vertex r2 = new Vertex();
        Vertex r3 = new Vertex();
        Vertex r4 = new Vertex();
        Vertex r5 = new Vertex();

        r1.setStep(new Step(r1, r2, 12));
        r1.setStep(new Step(r1, r4, 54));
        r2.setStep(new Step(r2, r3, 42));
        r2.setStep(new Step(r2, r5, 45));
        r3.setStep(new Step(r3, r4, 75));
        r3.setStep(new Step(r3, r4, 25));
        r4.setStep(new Step(r4, r3, 42));
        r4.setStep(new Step(r4, r2, 35));
        r5.setStep(new Step(r5, r1, 75));
        r5.setStep(new Step(r5, r2, 8));

        Trip trainTrip = new Trip("train");
        trainTrip.setTripValue(dk.getPathDis(v5));
        trainTrip.setPath(dk.getPath(v3));
        System.out.println(trainTrip);
    }


}
