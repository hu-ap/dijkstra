package src;

public class TrainRide extends Step {

    private double minutes;

    public TrainRide(Vertex source, Vertex destination, int weight, int minutes) {
        super(source, destination, weight);
        this.minutes = minutes;
    }
}
