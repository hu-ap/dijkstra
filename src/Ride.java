package src;

public class Ride extends Step {

    private double kilometers;

    public Ride(Vertex source, Vertex destination, int weight, double kilometers) {
        super(source, destination, weight);
        this.kilometers = kilometers;
    }
}
