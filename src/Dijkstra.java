package src;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

public class Dijkstra {

    public void calculatePath(Vertex source) {
        source.setMinDistance(0);
        PriorityQueue<Vertex> pq = new PriorityQueue<>();
        pq.add(source);

        while (!pq.isEmpty()) {
            Vertex vertex = pq.poll();

            for (Step step : vertex.getSteps()) {
                Vertex destination = step.getDestination();
                int weight = step.getWeight();
                double minDistance = vertex.getMinDistance() + weight;

                if(minDistance < destination.getMinDistance()) {
                    pq.remove(vertex);
                    destination.setPreviousVertex(vertex);
                    destination.setMinDistance(minDistance);
                    pq.add(destination);
                }
            }
        }
    }

    public double getPathDis(Vertex target) {
        return target.getMinDistance();
    }

    public List<Vertex> getPath(Vertex target) {
        List<Vertex> shortestPath = new ArrayList<>();

        for (Vertex step = target; target != null; target = target.getPreviousVertex()) {
            shortestPath.add(step);
        }

        Collections.reverse(shortestPath);
        return shortestPath;
    }
}
